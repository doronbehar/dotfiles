!! {{{ Xterm

!! {{{ Fonts
xterm*reportFonts:true
xterm*renderFont:true
xterm*faceName:UbuntuMonoDerivativePowerline Nerd Font
xterm*faceNameDoublesize:PowerlineSymbols
xterm*cacheDoublesize:1
xterm*faceSize:12
xterm*utf8:always
xterm*utf8Fonts:true
xterm*utf8Latin1:true
xterm*utf8Title:true
xterm*locale:true
!! }}}

!! {{{ Colors
xterm*background:black
xterm*foreground:white
xterm*color0:#2E3436
xterm*color1:#a40000
xterm*color2:#4E9A06
xterm*color3:#C4A000
xterm*color4:#3465A4
xterm*color5:#75507B
xterm*color6:#ce5c00
xterm*color7:#babdb9
xterm*color8:#555753
xterm*color9:#EF2929
xterm*color10:#8AE234
xterm*color11:#FCE94F
xterm*color12:#729FCF
xterm*color13:#AD7FA8
xterm*color14:#fcaf3e
xterm*color15:#EEEEEC
!! }}}

!! {{{ Behaviour
xterm*metaSendsEscape:true
xterm*loginShell:true
xterm*title:xterm
xterm*toolBar:false
xterm*termName:xterm-256color
xterm*cursorBlink:true
xterm*cursorOffTime:400
xterm*cursorOnTime:400
xterm*activeIcon:true
xterm*sameName:true
XTerm*allowWindowOps:true
!! }}}

!! {{{ Keyboard
!! Taken from https://wiki.archlinux.org/index.php/Xterm#PRIMARY_and_CLIPBOARD
xterm*VT100.translations:#override <Btn1Up>:select-end(PRIMARY, CLIPBOARD, CUT_BUFFER0)
!xterm.VT100.translations:#override \
!	Shift <KeyPress> Prior			:ignore() \n\
!	Shift <KeyPress> Next			:ignore() \n\
!	Shift <KeyPress> Select			:select-cursor-start() \
!									 select-cursor-end(SELECT, CUT_BUFFER0) \n\
!	Shift <KeyPress> Insert			:insert-selection(SELECT, CUT_BUFFER0) \n\
!	Alt <KeyPress>Return			:fullscreen() \n\
!	<KeyRelease> Scroll_Lock		:scroll-lock() \n\
!	Shift~Ctrl <KeyPress> KP_Add	:larger-vt-font() \n\
!	Shift Ctrl <KeyPress> KP_Add	:smaller-vt-font() \n\
!	Shift <KeyPress> KP_Subtract	:smaller-vt-font() \n\
!	~Meta <KeyPress>				:insert-seven-bit() \n\
!	Meta <KeyPress>					:insert-eight-bit() \n\
!	!Ctrl <Btn1Down>				:popup-menu(mainMenu) \n\
!	!Lock Ctrl <Btn1Down>			:popup-menu(mainMenu) \n\
!	!Lock Ctrl @Num_Lock <Btn1Down>	:popup-menu(mainMenu) \n\
!	! @Num_Lock Ctrl <Btn1Down>		:popup-menu(mainMenu) \n\
!	~Meta <Btn1Down>				:select-start() \n\
!	~Meta <Btn1Motion>				:select-extend() \n\
!	!Ctrl <Btn2Down>				:popup-menu(vtMenu) \n\
!	!Lock Ctrl <Btn2Down>			:popup-menu(vtMenu) \n\
!	!Lock Ctrl @Num_Lock <Btn2Down>	:popup-menu(vtMenu) \n\
!	! @Num_Lock Ctrl <Btn2Down>		:popup-menu(vtMenu) \n\
!	~Ctrl ~Meta <Btn2Down>			:ignore() \n\
!	Meta <Btn2Down>					:clear-saved-lines() \n\
!	~Ctrl ~Meta <Btn2Up>			:insert-selection(SELECT, CUT_BUFFER0) \n\
!	!Ctrl <Btn3Down>				:popup-menu(fontMenu) \n\
!	!Lock Ctrl <Btn3Down>			:popup-menu(fontMenu) \n\
!	!Lock Ctrl @Num_Lock <Btn3Down>	:popup-menu(fontMenu) \n\
!	! @Num_Lock Ctrl <Btn3Down>		:popup-menu(fontMenu) \n\
!	~Ctrl ~Meta <Btn3Down>			:start-extend() \n\
!	~Meta <Btn3Motion>				:select-extend() \n\
!	Ctrl <Btn4Down>					:scroll-back(1,halfpage,m) \n\
!	Lock Ctrl <Btn4Down>			:scroll-back(1,halfpage,m) \n\
!	Lock @Num_Lock Ctrl <Btn4Down>	:scroll-back(1,halfpage,m) \n\
!	@Num_Lock Ctrl <Btn4Down>		:scroll-back(1,halfpage,m) \n\
!	<Btn4Down>						:scroll-back(5,line,m)     \n\
!	Ctrl <Btn5Down>					:scroll-forw(1,halfpage,m) \n\
!	Lock Ctrl <Btn5Down>			:scroll-forw(1,halfpage,m) \n\
!	Lock @Num_Lock Ctrl <Btn5Down>	:scroll-forw(1,halfpage,m) \n\
!	@Num_Lock Ctrl <Btn5Down>		:scroll-forw(1,halfpage,m) \n\
!	<Btn5Down>						:scroll-forw(5,line,m)     \n\
!	<BtnUp>							:select-end(SELECT, CUT_BUFFER0) \n\
!	<BtnDown>						:ignore()
!! }}}

!! }}}

!! {{{ URXVT

!! {{{ Fonts
URxvt*font:\
	xft:DroidSansMonoForPowerline Nerd Font:size=10,\
	xft:DejaVuSansMonoForPowerline Nerd Font:size=9,\
	xft:DejaVuSansMonoForPowerline Nerd Font:size=8,\
	xft:Miriam Mono CLM:size=11,\
	xft:Miriam Mono CLM:size=10,\
	xft:Miriam Mono CLM:size=9,\
	xft:Symbola:size=9,\
	xft:Symbola:size=8,\
	xft:Symbola:size=7
!! }}}

!! {{{ Colors
URxvt*scrollBar:false
URxvt*saveLines:65535
URxvt.background:#000000
URxvt*foreground:#ffffff
URxvt*color0:#2E3436
URxvt*color1:#a40000
URxvt*color2:#4E9A06
URxvt*color3:#C4A000
URxvt*color4:#3465A4
URxvt*color5:#75507B
URxvt*color6:#ce5c00
URxvt*color7:#babdb9
URxvt*color8:#555753
URxvt*color9:#EF2929
URxvt*color10:#8AE234
URxvt*color11:#FCE94F
URxvt*color12:#729FCF
URxvt*color13:#AD7FA8
URxvt*color14:#fcaf3e
URxvt*color15:#EEEEEC
URxvt.url-select.underline:false
URxvt.underlineURLs:False
!! }}}

!! {{{ Behaviour
URxvt*loginShell:true
URxvt.perl-ext:url-select,keyboard-select,font-size
Urxvt.bidi.FieldSep:\x{2502}
URxvt.url-launcher:/usr/bin/xdg-open
URxvt.url-select.launcher:/usr/bin/xdg-open
URxvt*cursorBlink:true
URxvt*cursorOffTime:400
URxvt*cursorOnTime:400
! do not scroll with output
URxvt*scrollTtyOutput:false
! scroll back to the bottom on keypress
URxvt*scrollTtyKeypress:true
!! }}}

!! {{{ Keyboard
!! Taken from https://github.com/neovim/neovim/issues/2269#issuecomment-202440562:
URxvt.keysym.Control-Up:\033[1;5A
URxvt.keysym.Control-Down:\033[1;5B
URxvt.keysym.Control-Left:\033[1;5D
URxvt.keysym.Control-Right:\033[1;5C
!! Taken from http://unix.stackexchange.com/a/46376/135796
URxvt.keysym.Shift-Up:command:\033]720;1\007
URxvt.keysym.Shift-Down:command:\033]721;1\007
!! Taken from https://wiki.gentoo.org/wiki/Rxvt-unicode#Printing
! The string will be interpreted as if typed into the shell as-is.
! In this example, printing will be disabled altogether.
URxvt.print-pipe:"cat > /dev/null"
URxvt.keysym.C-Delete:perl:url-select:select_next
URxvt.keysym.M-Delete:perl:keyboard-select:activate
URxvt.keysym.M-Escape:perl:keyboard-select:search
URxvt.keysym.M-Tab:perl:font-size:increase
URxvt.keysym.M-Home:perl:font-size:reset
URxvt.keysym.M-End:perl:font-size:show
!! }}}

!! }}}

!! vim:foldmethod=marker
